<?php
require_once"../model/consult.php";
session_start(); 

$route = $_REQUEST['accion'];

switch ($route) {

	case 'validate':
		$user     = $_REQUEST['nombre'];
		$password = $_REQUEST['password'];
		$obj      = new consult();
		$resp     = $obj->validateUser($user, $password);
		if($resp['estado'] == 200){

			$_SESSION['nombres'] = $resp['data']['nombres'];
			$_SESSION['apellidos'] = $resp['data']['apellidos'];
			$_SESSION['usuario'] = $resp['data']['usuario'];
			//print_r($_SESSION['usuario']);
		}
		echo json_encode($resp);
	break;

	case 'add':
		$obj      = new consult();
		$resp     = $obj->createUser($_REQUEST);
		echo json_encode($resp);
	break;

	case 'list':
		$obj     = new consult();
		$resp    = $obj->getListPeople();
		echo json_encode($resp);
		break;

	case 'edit':
		$id      = $_REQUEST['user_id'];
		$obj     = new consult();
		$resp    = $obj->getDataPeople($id);
		echo json_encode($resp);
	break;
	
	case 'update':
		$obj     = new consult();
		$resp    = $obj->UpdatePeople($_REQUEST);
		echo json_encode($resp);
	break;

	case 'closed':
		session_destroy(); 
		unset ($_SESSION['nombres']);
		unset ($_SESSION['apellidos']);
		unset ($_SESSION['usuario']);
		$resp = 200;
		echo json_encode($resp);
	break;

}
