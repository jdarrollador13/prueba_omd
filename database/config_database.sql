----------- POSTGRES ----------------
CREATE DATABASE omd
WITH OWNER    		  = postgres
	 ENCODING 		  = 'UTF8'
	 TABLESPACE 	  = pg_default
	 CONNECTION LIMIT = -1;

--table postgres
CREATE TABLE registro_persona(
	id  SERIAL PRIMARY KEY NOT NULL,
	cedula INT NUMERIC,
	nombres TEXT NOT NULL,
	apellidos TEXT NOT NULL,
	correo TEXT NOT NULL,
	eps TEXT NOT NULL,
	edad TEXT NOT NULL,
	telefono TEXT NOT NULL,
	nombre_empresa TEXT NOT NULL,
	fecha TEXT NOT NULL
)

-- Table: public.usuarios

-- DROP TABLE public.usuarios;

CREATE TABLE public.usuarios
(
  id SERIAL PRIMARY KEY NOT NULL,
  nombres TEXT NOT NULL,
  apellidos TEXT NOT NULL,
  usuario TEXT NOT NULL,
  password TEXT NOT NULL,
)

INSERT INTO usuarios (nombres, apellidos, usuario, password) VALUES ('admin', 'admin', 'admin', 'admin');

------- MYSQL -----------

-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-08-2019 a las 08:40:07
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `omd`
--
CREATE DATABASE IF NOT EXISTS `omd` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `omd`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_persona`
--

DROP TABLE IF EXISTS `registro_persona`;
CREATE TABLE `registro_persona` (
  `id` int(11) NOT NULL,
  `cedula` text COLLATE utf8_spanish_ci NOT NULL,
  `nombres` text COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` text COLLATE utf8_spanish_ci NOT NULL,
  `correo` text COLLATE utf8_spanish_ci NOT NULL,
  `eps` text COLLATE utf8_spanish_ci NOT NULL,
  `edad` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono` text COLLATE utf8_spanish_ci NOT NULL,
  `nombre_empresa` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombres` text COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` text COLLATE utf8_spanish_ci NOT NULL,
  `usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `password` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombres`, `apellidos`, `usuario`, `password`) VALUES(1, 'admin', 'admin', 'admin', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `registro_persona`
--
ALTER TABLE `registro_persona`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `registro_persona`
--
ALTER TABLE `registro_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
