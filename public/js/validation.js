	// Wait for the DOM to be ready
	$(function() {
		console.log('rrrrrrrr')
	  // Initialize form validation on the registration form.
	  // It has the name attribute "registration"
	  $("#formadd").validate({
	    // Specify validation rules
	    rules: {
	      // The key name on the left side is the name attribute
	      // of an input field. Validation rules are defined
	      // on the right side
	      nombres: "required",
	      apellidos: "required",
	      cedula: "required",
	      eps: "required",
	      edad: {
	      	required : true,
	      	number: true
	      },
	      correo: {
	        required: true,
	        // Specify that email should be validated
	        // by the built-in "email" rule
	        email: true
	      },
	      telefono: {
	      	required : true,
	      	number: true
	      },
	      empresa:"required",
	    },
	    // Specify validation error messages
	    messages: {
	      nombres: "El campo nombres es obligatorio.",
	      apellidos: "El campo apellidos es obligatorio",
	      cedula: "El campo cedula es obligatorio",
	      eps: "El campo eps es obligatorio",
	      edad: "El campo dad es obligatorio",
	      telefono: "El campo telefono es obligatorio",
	      empresa: "El campo Empresa es obligatorio"
	    },
	    // Make sure the form is submitted to the destination defined
	    // in the "action" attribute of the form when valid
	    submitHandler: function(form) {
	    	console.log(form)
	      form.submit();
	    }
	  });
	});