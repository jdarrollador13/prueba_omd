$(document).ready(function(){
	var data = localStorage.getItem("data_update");
	var _data = JSON.parse(data);
	document.getElementById("id").value = _data.data.id;
	document.getElementById("nombres").value = _data.data.nombres;
 	document.getElementById("apellidos").value = _data.data.apellidos;
 	document.getElementById("cedula").value = _data.data.cedula;
 	document.getElementById("correo").value = _data.data.correo;
 	document.getElementById("eps").value = _data.data.eps;
 	document.getElementById("edad").value = _data.data.edad;
 	document.getElementById("telefono").value = _data.data.telefono;
 	document.getElementById("empresa").value = _data.data.nombre_empresa;

 	$("#formadd").submit(function(e) {
		e.preventDefault();
	}).validate({
		debug: false,
    rules: {
      nombres: "required",
      apellidos: "required",
      cedula: "required",
      eps: "required",
       edad: {
          required : true,
          number: true
      },
      correo: {
        required: true,
        email: true
      },
      telefono: {
          required : true,
          number: true
      },
      empresa:"required",
    },
    messages: {
      nombres: "El campo nombres es obligatorio.",
      apellidos: "El campo apellidos es obligatorio",
      cedula: "El campo cedula es obligatorio",
      eps: "El campo eps es obligatorio",
      edad: "El campo edad es obligatorio y es tipo numerico",
      correo: "El campo correo es obligatorio",
      telefono: "El campo telefono es obligatorio y es tipo numerico",
      empresa: "El campo Empresa es obligatorio"
    },
    submitHandler: function (form) { 
			var objPersona = {
				id   : document.getElementById("id").value,
				nombre   : document.getElementById("nombres").value,
				apellidos: document.getElementById("apellidos").value,
				cedula   : document.getElementById("cedula").value,
				correo   : document.getElementById("correo").value,
				eps      : document.getElementById("eps").value,
				edad     : document.getElementById("edad").value,
				telefono : document.getElementById("telefono").value,
				empresa  : document.getElementById("empresa").value
			}
			console.log(objPersona)
			$.ajax({
        url: '../../prueba_omd/controller/route.php?accion=update',
        type: 'POST',
        data: objPersona,
        success: function(data){
        	console.log(data)
        	response = JSON.parse(data);
        	if(response.estado == 200 ){
        		window.location="principal.php";
        	}else if(response.estado == 201){
        	}
        
       	},
	      error: function() {
	        alert('There was some error performing the AJAX call!');
	      }
      });
      return false; 
    }
  });
		   	
});