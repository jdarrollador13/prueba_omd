$(document).ready(function() {

	$("#formadd").submit(function(e) {
		e.preventDefault();
	}).validate({
		debug: false,
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      nombres: "required",
      apellidos: "required",
      cedula: "required",
      eps: "required",
      edad: {
          required : true,
          number: true
      },
      correo: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      telefono: {
          required : true,
          number: true
      },
      empresa:"required",
    },
    // Specify validation error messages
    messages: {
      nombres: "El campo nombres es obligatorio.",
      apellidos: "El campo apellidos es obligatorio",
      cedula: "El campo cedula es obligatorio",
      eps: "El campo eps es obligatorio",
      edad: "El campo edad es obligatorio y es tipo numerico",
      correo: "El campo correo es obligatorio",
      telefono: "El campo telefono es obligatorio y es tipo numerico",
      empresa: "El campo Empresa es obligatorio"
    },
    submitHandler: function (form) { 
			var objPersona = {
				nombre    	 : document.getElementById("nombres").value,
				apellidos 	 : document.getElementById("apellidos").value,
				cedula   : document.getElementById("cedula").value,
				correo   : document.getElementById("correo").value,
				eps      : document.getElementById("eps").value,
				edad     : document.getElementById("edad").value,
				telefono : document.getElementById("telefono").value,
				empresa   : document.getElementById("empresa").value
			}
			console.log(objPersona)
			$.ajax({
        url: '../../prueba_omd/controller/route.php?accion=add',
        type: 'POST',
        data: objPersona,
        success: function(data){
        	var response = JSON.parse(data);
        	if(response.estado == 200 ){
        		window.location="principal.php";
        	}else if(response.estado == 201){
        		window.location="view/add.php";
        		//window.location="index.php";
        	}
        
       	},
	      error: function() {
	        alert('There was some error performing the AJAX call!');
	      }
      });
      return false; // for demo
    }
  });

});