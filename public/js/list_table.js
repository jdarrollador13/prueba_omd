$(document).ready(function(){
	$.ajax({
    url: '../../prueba_omd/controller/route.php?accion=list',
    type: 'GET',
    success: function(data){
    	var response = JSON.parse(data);
    	if(response.estado == 200 ){
    		var html ="";
    		for(var x in response.data){
    			html += "<tr>";
    			html += "<td class='pt-3-half' id='id-table' contenteditable='false'>"+ response.data[x].id +"</td>";
    			html += "<td class='pt-3-half' contenteditable='false'>"+ response.data[x].nombres +"</td>";
    			html += "<td class='pt-3-half' contenteditable='false'>"+ response.data[x].apellidos +"</td>";
    			html += "<td class='pt-3-half' contenteditable='false'>"+ response.data[x].correo +"</td>";
    			html += "<td class='pt-3-half' contenteditable='false'>"+ response.data[x].telefono +"</td>";
    			html += "<td class='pt-3-half' contenteditable='false'>"+ response.data[x].nombre_empresa +"</td>";
    			html += "<td class='pt-3-half' contenteditable='false'>"+ response.data[x].eps +"</td>";
    			html += "<td class='pt-3-half'>";
    			html += "<span class='table-remove'><a href='#' class='text-success update' id='"+ response.data[x].id +"'>Editar</a></span>";
    			html += "</td>";
    			$( "#tbody-render" )
    			.html(html);
    			//'../../../prueba_omd/controller/route.php?accion=edit&id='"+response.data[x].id+"
    		}
    	}else if(response.estado == 201){
    		var html ="";
    		html += "<tr>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half' contenteditable='false'></td>";
  			html += "<td class='pt-3-half'>";
  			html += "<span class='table-remove'><a href='#' class='text-success>Editar</a></span>";
  			html += "</td>";
  			$( "#tbody-render" )
    		.html(html);
    	}
    
   	},
    error: function() {
      alert('There was some error performing the AJAX call!');
    }
  });

  $(document).on('click', '.update', function(){
	  var user_id = $(this).attr("id");
	  console.log(user_id)
	  $.ajax({
		   url:"../../prueba_omd/controller/route.php?accion=edit",
		   method:"POST",
		   data:{user_id:user_id},
		   success:function(data)
		   {
		   	var _data = JSON.parse(data); 
		   	localStorage.setItem("data_update","");
		   	localStorage.setItem("data_update",data);
		   	window.location="../view/edit.php"
		   },error: function(){
		   	alert('There was some error performing the AJAX call!');
		   }
	  })
 	});
});