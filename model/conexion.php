<?php

class Conexion{

	private $host;
	private $pass;
	private $db;
	private $password;
	public  $connect;
	public  $port;

	public function __construct(){
		$this->host 		= "localhost";
		$this->user 		= "postgres";
		$this->db 			= "omd";
		$this->password = "admin";
		$this->port     = "5432";
	}

	public function conectar (){
		try {
			//$this->connect = new PDO('mysql:host=localhost;dbname=omd', 'root', '');
			$this->connect = new PDO('pgsql:host=localhost;port=5432;dbname=omd', 'postgres', 'admin');
		} catch (Exception $e) {
			echo $e->getMessage;
		}
	
	}

	public function desconectar (){
		$this->connect = pg_close($this->connect);
	}
}