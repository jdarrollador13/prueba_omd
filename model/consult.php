<?php

require_once"conexion.php";

class Consult {

	private $sql;
	private $query;
	private $data;

	public function __construct(){
		$this->connect = new Conexion();
		$this->connect->conectar();

	}

	public function validateUser ($user, $password){
		$this->data = Array();
		
		try {
			$this->sql="SELECT * FROM usuarios WHERE usuario = '$user' AND password = '$password'";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			$this->data['data']    = $this->query->fetch();
			if($this->query->rowCount() > 0 ){
				$this->data['estado'] = 200;
			}else{
				$this->data['estado'] = 201;
			}
			$this->data['mensaje'] = 'ok';
			return $this->data;
		} catch (Exception $e) {
			echo $e->getMessage();
			$this->data['mensaje'] = 'error';
			$this->data['estado'] = 500;
			return $this->data;
		}
		
	}

	public function createUser($request){
		date_default_timezone_set('America/Bogota');
		$this->data = Array();
		$cedula    = $request['cedula'];
		$nombre    = $request['nombre'];
		$apellidos = $request['apellidos'];
		$correo    = $request['correo'];
		$eps       = $request['eps'];
		$edad      = $request['edad'];
		$telefono  = $request['telefono'];
		$nombre_empresa      = $request['empresa'];
		$fecha  = date('Y-m-d H:i'); 
		try {
			$this->sql="INSERT INTO registro_persona(cedula,nombres,apellidos,correo,eps,edad,telefono,nombre_empresa,fecha) 
								VALUES ('$cedula','$nombre','$apellidos','$correo','$eps', '$edad', '$telefono','$nombre_empresa','$fecha') ";
		
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();

			if($this->query->rowCount() > 0){
				$this->data['estado'] = 200;
			}else{
				$this->data['estado'] = 201;
			}
			$this->data['mensaje'] = "ok";
			return $this->data;
		} catch (Exception $e) {
			echo $e->getMessage();
			$this->data['estado'] = 500;
			$this->data['mensaje'] = "error";
			return $this->data;
		}
	}

	public function getListPeople(){

		try {
			$this->sql ="SELECT * FROM registro_persona ";
			$this->query = $this->connect->connect->prepare($this->sql);

			$this->query->execute();
			$this->data['data']    = $this->query->fetchAll();
			if($this->query->rowCount() > 0 ){
				$this->data['estado']  = 200;
			}else{
				$this->data['estado'] = 201;
			}
			return $this->data;
		} catch (Exception $e) {
			echo $e->getMessage();
			$this->data['estado'] = 500;
			$this->data['mensaje'] = "error";
			return $this->data;
		}
	}

	public function getDataPeople($id){
		try {
			$this->sql = "SELECT * FROM registro_persona WHERE id= $id";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();

			$this->data['data'] = $this->query->fetch();
			if($this->query->rowCount() > 0){
				$this->data['estado'] = 200;
			}else{
				$this->data['estado'] = 200;
			}
			$this->data['mensaje'] = "ok";
			return $this->data;
		} catch (Exception $e) {
			echo $e->getMessage();
			$this->data['estado'] = 500;
			$this->data['mensaje'] = "error";
			return $this->data;
		}
	}

	public function UpdatePeople($request){
		date_default_timezone_set('America/Bogota');
		$this->data = Array();
		$id    = $request['id'];
		$cedula    = $request['cedula'];
		$nombre    = $request['nombre'];
		$apellidos = $request['apellidos'];
		$correo    = $request['correo'];
		$eps       = $request['eps'];
		$edad      = $request['edad'];
		$telefono  = $request['telefono'];
		$nombre_empresa      = $request['empresa'];
		$fecha  = date('Y-m-d H:i'); 
		try {
			$this->sql = "UPDATE registro_persona SET cedula='$cedula',nombres='$nombre',apellidos='$apellidos',correo='$correo',eps='$eps',
									 edad = '$edad',telefono = '$telefono',nombre_empresa = '$nombre_empresa',fecha = '$fecha' WHERE id=$id";
			$this->query  = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if($this->query->rowCount() > 0){
				$this->data['estado'] = 200;
			}else{
				$this->data['estado'] = 201;
			}
			$this->data['mensaje'] = "ok";

			return $this->data;
		} catch (Exception $e) {
			echo $e->getMessage();
			$this->data['estado'] = 500;
			$this->data['mensaje'] = "error";
			return $this->data;
		}
	}

}