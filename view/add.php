<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Agregar</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../public/css/fontawesome/font-awesome.min.css">
    <script src='../public/js/fontawesome/kit_fontawesome.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
    <style>
      .header {
        color: #36A0FF;
        font-size: 27px;
        padding: 10px;
      }
      .bigicon {
        font-size: 24px;
        color: #36A0FF;
      }
      .card-form{
        width: 50%;
        margin-left: 311px;
        margin-top: 28px;
      }
      .row-form{
        margin-left: -74px;;
      }
      .btn-form{
        width: 204px;
        height: 45px;
      }
      html,body{
        background-image: url('../public/images/home3.png');
        /*background-size: cover;*/
        background-size: 100% 145%;;
        background-repeat: no-repeat;
        height: 100%;
      }
    </style>
</head>
  <body>
    <nav class="navbar  navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
      <a class="navbar-brand" href="principal.php" style="font-weight: bold; font-size: 14px;">Inicio</a>
      <a class="navbar-brand" href="list.php" style="font-weight: bold; font-size: 14px;">Listar Personal</a>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="nav-link disabled" href="#" style="font-weight: bold;"></a>
          <a class="navbar-brand" id="cerrar-sesion" href="#" tabindex="-1" style="font-weight: bold; font-size: 16px;">Cerrar Sessión</a>
          <a class="navbar-brand" href="#">
            <img src="../public/images/icono.png" width="60" height="50" class="d-inline-block align-top" alt="" style="border-radius: 10px;">
          </a>
        </form>
      </div>
    </nav>
    <div class="card card-form">
      <div class="card-header">
          <h4 style="margin-left: 198px;">Formulario de Registro</h4>
      </div>
      <div class="card-body">
        <blockquote class="blockquote mb-0">
            <form method="post" name="formadd" id="formadd" action="javascript:void(0)">

              <fieldset>
                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="nombres" name="nombres" type="text" placeholder="Nombres" class="form-control">
                    </div>
                </div>

                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="apellidos" name="apellidos" type="text" placeholder="Apellidos" class="form-control">
                    </div>
                </div>
                
                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="cedula" name="cedula" type="text" placeholder="Cedula" class="form-control">
                    </div>
                </div>

                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="correo" name="correo" type="text" placeholder="Correo" class="form-control">
                    </div>
                </div>

                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="eps" name="eps" type="text" placeholder="EPS" class="form-control">
                    </div>
                </div>
                
                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="edad" name="edad" type="text" placeholder="Edad" class="form-control">
                    </div>
                </div>
                
                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="telefono" name="telefono" type="text" placeholder="Teléfono" class="form-control">
                    </div>
                </div>
                
                <div class="form-group row row-form">
                    <span class="col-md-1 offset-md-1 text-center"></span>
                    <div class="col-md-9">
                        <input id="empresa" name="empresa" type="text" placeholder="Empresa" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg btn-form" name="accion" id="save-people" value="add_users">Guardar</button>
                  </div>
                </div>
              </fieldset>
            
            
          </form>
           
        </blockquote>
      </div>
    </div>
    <script src='../public/js/validate_people.js'></script>
    <script src="../public/js/clossed.js"></script>
  </body>
</html>