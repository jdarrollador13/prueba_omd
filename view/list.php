<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Listar</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <style>
      html,body{
        background-image: url('../public/images/home3.png');
        /*background-size: cover;*/
        background-size: 100% 125%;;
        background-repeat: no-repeat;
        height: 100%;
      }
    </style>
  </head>
  <style>
    .card{
      width: 86% !important;
      margin-left: 106px !important;
      margin-top: 52px !important;
    }
  </style>
  <body>
    <nav class="navbar  navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
      <a class="navbar-brand" href="principal.php" style="font-weight: bold; font-size: 14px;">Inicio</a>
      <a class="navbar-brand" href="add.php" style="font-weight: bold; font-size: 14px;">Crear Personal</a>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="nav-link disabled" href="#" style="font-weight: bold;"></a>
          <a class="navbar-brand" id="cerrar-sesion" href="#" tabindex="-1" style="font-weight: bold; font-size: 16px;">Cerrar Sessión</a>
          <a class="navbar-brand" href="#">
            <img src="../public/images/icono.png" width="60" height="50" class="d-inline-block align-top" alt="" style="border-radius: 10px;">
          </a>
        </form>
      </div>
    </nav>
    <div class="row" style="width: 100% !important;">
      <div class="col-md-12">
        <section class="panel"> 
          <div class="panel-body">
            <div class="card" style="margin-top: 16px !important;">
              <h3 class="card-header text-center font-weight-bold text-uppercase py-4" style="font-size: 18px;">Personal Externo</h3>
              <div class="card-body">
                <div id="table">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    
                        <thead>
                          <tr>
                            <th class="text-center">Id</th>
                            <th class="text-center">Nombres</th>
                            <th class="text-center">Apellidos</th>
                            <th class="text-center">email</th>
                            <th class="text-center">Telefono</th>
                            <th class="text-center">Empresa</th>
                            <th class="text-center">EPS</th>
                            <th class="text-center">Acciones</th>
                          </tr>
                        </thead>
                        
                        <tbody id="tbody-render">
                        </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td class="gutter">
                          <div class="line number1 index0 alt2" style="display: none;">1</div>
                        </td>
                        <td class="code">
                          <div class="container" style="display: none;">
                            <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- Editable table -->
          </div>
        </section>
      </div>
    </div>
   <script src="../public/js/table.js"></script>
   <script src="../public/js/list_table.js"></script>
   <script src="../public/js/clossed.js"></script>
  </body>
</html>
