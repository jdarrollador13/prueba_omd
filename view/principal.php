<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Principal</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
      html,body{
        background-image: url('../public/images/home3.png');
        /*background-size: cover;*/
        background-size: 100% 100%;
        background-repeat: no-repeat;
        height: 100%;
        /*font-family: 'Numans', sans-serif;*/
      }
    </style>
  <body>
  <!--Navbar -->
    <nav class="navbar  navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
      <a class="navbar-brand" href="principal.php" style="font-weight: bold; font-size: 14px;">Inicio</a>
      <a class="navbar-brand" href="list.php" style="font-weight: bold; font-size: 14px;">Listar Personal</a>
      <a class="navbar-brand" href="add.php" style="font-weight: bold; font-size: 14px;">Registrar personal</a>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a class="nav-link disabled" href="#" style="font-weight: bold;"></a>
          <a class="navbar-brand" id="cerrar-sesion" href="#" tabindex="-1" style="font-weight: bold; font-size: 16px;">Cerrar Sessión</a>
          <a class="navbar-brand" href="#">
            <img src="../public/images/icono.png" width="60" height="50" class="d-inline-block align-top" alt="" style="border-radius: 10px;">
          </a>
        </form>
      </div>
    </nav>
    <?php

    ?>
<!--/.Navbar -->
  <script src="../public/js/clossed.js"></script>
  </body>
</html>
