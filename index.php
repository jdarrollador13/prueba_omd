<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Loguin</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="public/css/style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
  <body>
    <div class="container">
      <div class="d-flex justify-content-center h-100">
        
        <div class="card">
          
          <div class="alert alert-info error-loguin" id="error-loguin" role="alert">
            
          </div>
          <div class="card-header">
              <h3>Ingresar</h3>
          </div>
          <div class="card-body" style="margin-top: 13px;">
                <form action="post">
                  <div class="input-group form-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input type="text" name="name" id="name" class="form-control" placeholder="usuario">

                  </div>
                  <div class="input-group form-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-key"></i></span>
                      </div>
                      <input class="form-control" type="password" name="password" id="pasword" placeholder="clave">
                  </div>
                </form>
                  <div class="form-group">
                      <input class="btn float-right login_btn" type="submit" name="accion" id="loguin" value="Ingresar">
                  </div>
             
          </div>
          <div class="card-footer">
              
          </div>
        </div>
      </div>
    </div>
    <script src="public/js/authen.js"></script>
  </body>
</html>